import java.util.ArrayList;

/**
 * generic queues using java.util.ArrayList
 * 
 * @author Andre and Carlos
 */
public class ArrayListQueue<T> implements QueueInterface<T> {
    private ArrayList<T> _array;
    
    public ArrayListQueue () {
        _array = new ArrayList<T>();
    }
    
    
    /**
     * Returns true if queue is empty, false otherwise.
     * @return whether queue is empty
     */
    public boolean isEmpty() {
        return _array.isEmpty();  
    }

    
    /**
     * Appends specified element onto back of queue
     * @param element to be appended onto queue
     */
    public void enqueue(T element) {
        _array.add(element);
    }
    
    
    /**
     * Removes and returns element currently sitting at front of queue,
     * assuming it is not empty. (If empty, it generates a "queue empty"
     * error.)
     * @return element at front of queue that is removed
     */
    public T dequeue() {
        int n = 0;
        if (_array.isEmpty() == false){
            return _array.remove(n);
        } else {
            throw new RuntimeException("queue empty");
        }
        
    }
    
}
