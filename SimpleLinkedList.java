/**
 * simplified generic linked lists
 * 
 * @author <Carlos and Andre>
 */
public class SimpleLinkedList<T> implements SimpleListInterface<T>
{
    private class Node {
        public T value;
        public Node next;
        public Node(T value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    private Node _head;

    
    /**
     * Constructs an empty list.
     */
    public SimpleLinkedList() {
        _head = null;
    }

    
    /**
     * Returns number of elements on list.
     * @return number of elements on list
     */
    public int size() {
        int n = 0;
        for (Node node = _head; node!= null; node=node.next){
            n++;
        }
        return n;
    }

    
    /**
     * Returns element at specified position on list.
     * @param index position of element to return
     * @return element at specified position on list
     * @throws IndexOutOfBoundsException - if index < 0 or >= size()
     */
    public T get(int index) {
        int i = 0;
        
        throw new IndexOutOfBoundsException("index: " + index);
        // ...
    }

    
    /**
     * Replaces whatever was at specified position on list with
     * specified element.
     * @param index position of element to return
     * @param element to put into list
     */
    public void set(int index, T element) {
      
        
        throw new IndexOutOfBoundsException("index: " + index);
        // ...
    }

    
    /**
     * Appends new element onto back of list.
     * @param element to appended onto list
     */
    public void append(T element) {
        if (_head == null){
            _head = new Node(element, null);    
        }else{
            Node runner = _head;
            if (runner.next != null) {
                runner = runner.next;
            } else {
                runner.next = new Node (element, null);
            }
        }        
    }
    
    /**
     * Returns position of element in list if present, otherwise returns
     * -1.
     * @param element item to be searched for
     * @return position of element in list or -1 if not found
     */
    public int indexOf(T element) {
        int pos = -1;
        // ...
        return pos;
    }

    
    /**
     * Returns string representation of list.
     * @return string representation of list
     */
    public String toString() {
        String s = "[";
        // ...
        s += "]";
        return s;
    }


    // ...
}
